﻿using UnityEngine;
using System.Collections;

//*****************************************************************************
//       Crucial Collider Gizmo by TimeFloat. Thanks for purchasing!         **
//*****************************************************************************

public enum CCGPresets
{
	Custom,
	Red,
	Blue,
	Green,
	Purple,
	Yellow
};

// Simply attach this script to any game object that has a collider that you would like
// to be drawn on screen and you are good to go!
[ExecuteInEditMode]
public class CrucialColliderGizmo : MonoBehaviour
{
	
	public CCGPresets selectedPreset;
	
	public Color savedCustomWireColor;
	public Color savedCustomFillColor;
	public Color savedCustomCenterColor;
	
	public float overallAlpha = 1.0f;
	public Color wireColor = new Color(.6f, .6f, 1f, .5f);
	public Color fillColor = new Color(.6f,.7f,1f,.1f);
	public Color centerColor = new Color(.6f,.7f,1f,.7f);
	
	public bool drawFill = true;
	public bool drawWire = true;
	
	public bool drawCenter = false;
	public float centerMarkerRadius = 1.0f;
	
	public float collider2D_ZDepth = 2.0f;
	
	public bool includeChildColliders = false;

    Transform[] allTransforms = new Transform[0];


    void OnEnable()
    {
        allTransforms = gameObject.GetComponentsInChildren<Transform>();
    }
    
    void OnDrawGizmos()
	{
		if(!enabled)
		{
			return;
		}

		DrawColliders(this.gameObject);		
		
		if(includeChildColliders)
		{
            for (int i = allTransforms.Length - 1; i >= 0; i--)
            {
                Transform curr = allTransforms[i];

                if (curr == null)
                {
                    OnEnable();
                    return;
                }

                if (curr.gameObject == gameObject)                
                    continue;
                
				 DrawColliders(curr.gameObject);
            }
		}
	}
	
	delegate void Drawer(CubeDrawer cubeDrawer, SphereDrawer sphereDrawer, CapsuleDrawer capsuleDrawer, MeshDrawer meshDrawer, bool isWire);
	
	delegate void CubeDrawer(Vector3 center, Vector3 size);
	delegate void SphereDrawer(Vector3 center, float radius);
    delegate void CapsuleDrawer(Vector3 start, Vector3 end, Color color, float radius);
    delegate void MeshDrawer(Mesh mesh, Vector3 position, Quaternion rotation, Vector3 scale);

    //BoxCollider 2D
    void DrawCollider(CubeDrawer drawer, BoxCollider2D collider, Vector3 position, Vector3 scale)
	{
		if (!collider) return;
		drawer(position + new Vector3(collider.offset.x, collider.offset.y, 0.0f), new Vector3 (collider.size.x * scale.x, collider.size.y * scale.y, collider2D_ZDepth));
	}
	
    //Box Collider
	void DrawCollider(CubeDrawer drawer, BoxCollider collider, Transform targetTran)
	{
		if (!collider) return;
		Gizmos.matrix = Matrix4x4.TRS(targetTran.position, targetTran.rotation, targetTran.lossyScale);
		drawer(collider.center, collider.size);
		Gizmos.matrix = Matrix4x4.identity;
	}

    //CircleCollider2D
    void DrawCollider(SphereDrawer drawer, CircleCollider2D collider, Vector3 position, Vector3 scale)
    {
        if (!collider) return;
        drawer(position + new Vector3(collider.offset.x, collider.offset.y, 0.0f), collider.radius * Mathf.Max(scale.x, scale.y));
    }

    //SphereCollider
	void DrawCollider(SphereDrawer drawer, SphereCollider collider, Vector3 position, Vector3 scale)
	{
		if (!collider) return;
		drawer(position + new Vector3(collider.center.x, collider.center.y, 0.0f), collider.radius * Mathf.Max(scale.x, scale.y, scale.z));
	}


    Vector3 diffHack;
    CapsuleCollider colliderHack;
    Mesh cylinderHack;
    //CapsuleCollider
    void DrawCollider(CapsuleDrawer drawer, CapsuleCollider collider, Vector3 position, bool isWire)
    {
        if (!collider || drawer == null) return;

        colliderHack = collider;
        diffHack = new Vector3(0, collider.height * .5f, 0);
        position += collider.center;

        if (isWire)
        {
            drawer(RotatePointAroundPivot(position - diffHack, position, collider.transform.localRotation.eulerAngles),
                    RotatePointAroundPivot(position + diffHack, position, collider.transform.localRotation.eulerAngles),
                    wireColor,
                    collider.radius);
        }
        else
        {
            DrawFilledCapsule(position - diffHack, position + diffHack, wireColor, collider.radius);
        }
    }

    Vector3 radiusDiff;
    Vector3 capsuleCenter;
    void DrawFilledCapsule(Vector3 start, Vector3 end, Color color, float radius)
    {
        radiusDiff = new Vector3(0, radius, 0);
        capsuleCenter = start + diffHack;
        Gizmos.DrawSphere(RotatePointAroundPivot(start + radiusDiff, capsuleCenter, colliderHack.transform.localRotation.eulerAngles), radius);
        if (cylinderHack == null)
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            cylinderHack = go.GetComponent<MeshFilter>().sharedMesh;
            DestroyImmediate(go);
        }
        Gizmos.DrawMesh(cylinderHack, start + diffHack, colliderHack.transform.rotation, new Vector3(radius * 2, diffHack.y - radius, radius * 2));
        Gizmos.DrawSphere(RotatePointAroundPivot(end - radiusDiff, capsuleCenter, colliderHack.transform.localRotation.eulerAngles), radius);
    }

    public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        Vector3 dir = point - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(angles) * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point; // return it
    }

    //MeshCollider
    void DrawCollider(MeshDrawer drawer, MeshCollider[] colliders, Vector3 position, Vector3 scale)
    {
        if (colliders == null || drawer == null) return;

        for (int i = colliders.Length - 1; i >= 0; i--)
        {
            drawer(colliders[i].sharedMesh, position, colliders[i].transform.rotation, scale);
        }
    }

    void DrawColliders(GameObject hostGameObject)
	{
		Transform targetTran = hostGameObject.transform;
		Vector3 position = targetTran.position;
		Vector3 trueScale = targetTran.localScale;
		while(targetTran.parent != null)
		{
			targetTran = targetTran.parent;
			trueScale = new Vector3(targetTran.localScale.x * trueScale.x, targetTran.localScale.y * trueScale.y, targetTran.localScale.z * trueScale.z);
		}
		
		Drawer draw = (CubeDrawer cubeDrawer, SphereDrawer sphereDrawer, CapsuleDrawer capsuleDrawer, MeshDrawer meshDrawer, bool isWire) =>			
		{
			DrawCollider(cubeDrawer, hostGameObject.GetComponent<BoxCollider2D>(), position, trueScale);
			DrawCollider(cubeDrawer, hostGameObject.GetComponent<BoxCollider>(), hostGameObject.transform);
			DrawCollider(sphereDrawer, hostGameObject.GetComponent<CircleCollider2D>(), position, trueScale);
			DrawCollider(sphereDrawer, hostGameObject.GetComponent<SphereCollider>(), position, trueScale);
            DrawCollider(capsuleDrawer, hostGameObject.GetComponent<CapsuleCollider>(), position, isWire);
            DrawCollider(meshDrawer, hostGameObject.GetComponents<MeshCollider>(), position, trueScale);
        };
		
		Gizmos.color = new Color(wireColor.r, wireColor.g, wireColor.b, wireColor.a * overallAlpha);
		if (drawWire)
		{
			draw(Gizmos.DrawWireCube, Gizmos.DrawWireSphere, DebugExtension.DrawCapsule, Gizmos.DrawWireMesh, true);
		}
		
		Gizmos.color = new Color(fillColor.r, fillColor.g, fillColor.b, fillColor.a * overallAlpha);
		if (drawFill)
		{
			draw(Gizmos.DrawCube, Gizmos.DrawSphere, DebugExtension.DrawCapsule, Gizmos.DrawMesh, false);
		}
		
		if(drawCenter)
		{
			Gizmos.color = new Color(centerColor.r, centerColor.g, centerColor.b, centerColor.a * overallAlpha);
			Gizmos.DrawSphere(hostGameObject.transform.position, centerMarkerRadius);
		}
	}

#if UNITY_EDITOR
    [UnityEditor.MenuItem("KinshipEntertainment/Purge Crucial Collider Lag &#K")]
    static void Purge ()
    {
        foreach (var lag in GameObject.FindObjectsOfType<CrucialColliderGizmo>())
            Destroy(lag);
    }
#endif
}