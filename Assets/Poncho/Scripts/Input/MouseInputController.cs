﻿using System;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

namespace Poncho.Input
{
    public class MouseInputController : MonoBehaviour
    {
        PonchoInputActions actions;
        public Action<Vector2> OnMouseMoved;


        protected void Start()
        {
            actions = new PonchoInputActions();
            actions.Player.MouseMove.performed += MouseMove;

            actions.Enable();
        }

        private void MouseMove(CallbackContext obj)
        {
            Vector2 position = obj.ReadValue<Vector2>();
            OnMouseMoved?.Invoke(position);
            
        }
    }
}
