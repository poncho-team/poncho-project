﻿using Poncho.Actors;
using UnityEngine;

namespace Poncho.Input
{

    public abstract class BaseInputController : MonoBehaviour
    {
        protected PonchoInputActions actions;
        protected BaseActorComponent[] components;

        private void Start()
        {
            actions = new PonchoInputActions();
            components = GetComponents<BaseActorComponent>();
            Setup();

            actions.Enable();
        }

        protected abstract void Setup();

        private void OnDisable()
        {
            actions?.Disable();
        }

        private void OnEnable()
        {
            actions?.Enable();
        }
    }
}
