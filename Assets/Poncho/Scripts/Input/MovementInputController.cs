﻿using Poncho.Actors;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

namespace Poncho.Input
{
    class MovementInputController : BaseInputController
    {
        BaseMovementComponent movementComponent;
        BaseRotationComponent rotationComponent;
        BaseAnimatorComponent animatorComponent;
        protected override void Setup()
        {
            actions.Player.Move.performed += Move_performed;
            actions.Player.Move.canceled += Move_canceled;
            foreach(BaseActorComponent component in components)
            {
                switch(component)
                {
                    case BaseMovementComponent c: movementComponent = c; break;
                    case BaseRotationComponent c: rotationComponent = c; break;
                    case BaseAnimatorComponent c: animatorComponent = c; break;
                }
            }
        }

        private void Move_canceled(CallbackContext obj)
        {
            movementComponent?.OnMove(Vector2.zero);
            animatorComponent?.OnStopMoving();
            rotationComponent?.OnStopRotation();
        }

        private void Move_performed(CallbackContext obj)
        {
            Vector2 input = obj.ReadValue<Vector2>();
            movementComponent?.OnMove(input);
            rotationComponent?.OnRotate(input);
            animatorComponent?.OnMove(input);
        }

        
    }
}
