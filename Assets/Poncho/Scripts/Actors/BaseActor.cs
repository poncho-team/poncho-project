﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Poncho.Datas;

namespace Poncho.Actors
{
    public abstract class BaseActor : MonoBehaviourPun
    {
        public DataSet DataSet;
        public GameObject Visual { get; private set; }
        //TODO: Map this automatically
        BaseActorComponent[] components;

        public Vector3 Position
        {
            get => transform.position;
            set
            {
                transform.position = value; 
            }
        }

        public Quaternion Rotation
        {
            get { return transform.rotation; }
            set { transform.rotation = value; }
        }

        private void Awake()
        {
            SetVisual();
            components = GetComponents<BaseActorComponent>();
            foreach (BaseActorComponent component in components)
            {
                if (component.enabled)
                {
                    component.Setup(this);
                }
            }
        }
        //TODO: make it a component
        public virtual void SetVisual()
        {
            Visual = GameObject.Instantiate(DataSet.Get<VisualData>().Prefab, transform, false);
        }
    }
}

