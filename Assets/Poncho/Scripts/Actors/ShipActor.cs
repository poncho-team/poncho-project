﻿using UnityEngine;

namespace Poncho.Actors
{
    [RequireComponent(typeof(ShipMovementComponent))]
    public class ShipActor : BaseActor
    {
        public override void SetVisual()
        {
            base.SetVisual();
            Visual.transform.localPosition += Vector3.forward;
        }
    }
}

