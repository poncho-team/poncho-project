﻿using NaughtyAttributes;
using Poncho.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Poncho.Actors
{
    class ExampleCharacterCamera : BaseActorCamera<PlayerActor>
    {

        public const float DefaultCameraAngle = 50f;
        public const float DefaultCameraOffset = 1f;
        public const float DefaultCameraMinDistance = 5f;
        public const float DefaultCameraMaxDistance = 20f;

        [Range(5, 30)]
        public float minDist = 5f;
        [Range(20, 80)]
        public float maxDist = 20f;

        public float pendulumOffset = 0f;
        public float maxPendulumAngle = 10f;
        public float startPendulumDistance = 10f;
        public float pendudumMultiplier = 0.5f;

        [Range(-10, 10)]
        public float forwardOffset = 1f;
        public float zoomSensitivity = 20.0f;
        public float zoomSpeed = 5.0f;
        public float maxPanningSpeed = 30f;
        public float minPanningSpeed = 7f;
        public Vector2 panningOffset;
        public float mousePanSpeed = 15f;
        public float areaToIgnoreOffset;
        //public Transform Target { get; protected set; }
        [Range(40, 60)]
        public float cameraAngle = 55;

        Vector3 offset;
        Vector3 velocity = Vector3.zero;
        Vector3 panVelocity = Vector3.zero;

        float yRotation;

        float lastDistance;

        public float CameraDistance
        {
            get => cameraDistance;
            private set
            {
                if (cameraDistance == value) return;
                cameraDistance = value;
            }
        }

        public float TargetCameraDist { get; private set; }

        public float TargetRotation { get; protected set; }
        public Vector3 TargetPosition
        {
            get => targetPosition;
            set
            {
                if (targetPosition == value) return;
                targetPosition = value;
            }
        }

        public Camera Camera { get; set; }

        public MouseInputController mouseInputController;
        [ShowNonSerializedField]
        private Vector3 lastPan;
        [ShowNonSerializedField]
        private float lerpSpeed;
        private float cameraDistance;
        private Vector3 targetPosition;

        protected override void Awake()
        {
            base.Awake();
            Camera = GetComponentInChildren<Camera>();
            offset = (Vector3.forward * forwardOffset);
            Camera.transform.localPosition = new Vector3(0, 0, -20);
            TargetPosition = target.Position;

            lastDistance = maxDist;
            CameraDistance = maxDist;
            TargetCameraDist = CameraDistance;
            yRotation = 0;
            TargetRotation = yRotation;
        }

        protected override void MoveCamera()
        {

            if (target == null)
                return;

            //HACK (testing only)

            TargetRotation = yRotation;
            TargetPosition = target.Position;

            transform.rotation = Quaternion.Euler(new Vector3(cameraAngle, Mathf.LerpAngle(transform.eulerAngles.y, TargetRotation, Time.deltaTime), 0f));

            TargetCameraDist = Mathf.Clamp(TargetCameraDist - (UnityEngine.Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity), minDist, maxDist);
            CameraDistance = Mathf.Lerp(CameraDistance, TargetCameraDist, Time.deltaTime * zoomSpeed);

            Camera.transform.localPosition = new Vector3(0f, 0f, -CameraDistance);
            
            if (Mathf.Abs(CameraDistance - lastDistance) > Mathf.Epsilon)
            {
                lastDistance = CameraDistance;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(cameraAngle, TargetRotation, 0)), Time.deltaTime);
            }

            Vector3 mousePan = Mathfx.Hermite(lastPan, ApplyMousePan(), Time.deltaTime * lerpSpeed);

            lastPan = mousePan;
            transform.position = TargetPosition + mousePan; //Vector3.SmoothDamp(transform.position, (TargetPosition + mousePan), ref velocity, Time.deltaTime * panningSpeed);

        }

        private Vector3 ApplyMousePan()
        {
            offset = (Vector3.forward * forwardOffset);
            Vector3 rawPos = UnityEngine.Input.mousePosition;
            Vector2 mousePos = new Vector2(rawPos.x / Screen.width, rawPos.y / Screen.height) * 2 - Vector2.one;
            mousePos = Vector3.ClampMagnitude(mousePos, 1);

            //lerpSpeed = panningSpeed - panningSpeed * (Mathf.Max(mousePos.magnitude-areaToIgnoreOffset, 0));
            if (mousePos.magnitude < areaToIgnoreOffset)
            {
                mousePos.x = 0;
                mousePos.y = 0;
            }

            Vector2 panOffset = (panningOffset * CameraDistance) / maxDist;
            
            Vector3 offsetPan = new Vector3(panOffset.x * mousePos.x, 0, panOffset.y * mousePos.y);

            Vector3 maxPan = new Vector3(panningOffset.x, 0, panningOffset.y);

            Vector3 desiredPanVector = offsetPan - lastPan;
            float formula = desiredPanVector.magnitude /  (maxPan.magnitude * 2);
            float applyingSpeed = Mathf.Clamp(maxPanningSpeed * formula, minPanningSpeed, maxPanningSpeed);
            lerpSpeed = Mathf.Lerp(lerpSpeed, applyingSpeed, Time.deltaTime);

            return offsetPan + offset;
        }

        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlaying) return;
#endif
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(TargetPosition + lastPan, 1);
        }

    }
}
