﻿using UnityEngine;

namespace Poncho.Actors
{
    class VisualFacingCameraComponent : BaseActorComponent
    {
        //Orient the camera after all movement is completed this frame to avoid jittering
        private void LateUpdate()
        {
            actor?.Visual?.transform.LookAt(actor.Visual.transform.position + Camera.main.transform.rotation * Vector3.forward,
                Camera.main.transform.rotation * Vector3.up);
        }
    }
}
