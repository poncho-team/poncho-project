﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poncho.Actors
{
    public class MarineCamera : BaseActorCamera<PlayerActor>
    {
        protected override void MoveCamera()
        {
            if (!cameraData) return;
            Vector3 offset = cameraData.Offset;
            Vector3 desiredPos = target.transform.position + offset;
            transform.rotation = Quaternion.Euler(cameraData.pitchAngle, 0, 0);
            transform.position = Vector3.Lerp(transform.position, desiredPos, cameraData.Dampening * Time.deltaTime);
        }
    }
}