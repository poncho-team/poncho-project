﻿using Poncho.Datas;
using UnityEngine;

namespace Poncho.Actors
{
    public abstract class BaseActorCamera<T> : MonoBehaviour where T : BaseActor
    {
        [SerializeField]
        protected T target;
        protected ActorCameraData cameraData;

        protected virtual void Awake()
        {
            if (target)
            {
                cameraData = target.DataSet.Get<ActorCameraData>();
            }
        }

        public virtual void SetTarget(T actor)
        {
            target = actor;
            cameraData = actor.DataSet.Get<ActorCameraData>();
        }

        private void LateUpdate()
        {
            MoveCamera();
        }

        protected abstract void MoveCamera();
    }
}