﻿using UnityEngine;

namespace Poncho.Actors
{
    public class ShipCamera : BaseActorCamera<ShipActor>
    {
        protected override void MoveCamera()
        {
            if (!cameraData) return;
            Vector3 offset = cameraData.Offset;
            //if (target.transform.rotation.eulerAngles.y > 100)
            //    offset += Vector3.forward * offset.z * 2;
            Vector3 desiredPos = target.transform.position + offset;
            transform.rotation = Quaternion.Euler(cameraData.pitchAngle, 0, 0);
            transform.position = Vector3.Lerp(transform.position, desiredPos, cameraData.Dampening * Time.deltaTime);
        }
    }
}