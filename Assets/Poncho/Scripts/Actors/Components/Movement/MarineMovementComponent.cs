﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Poncho.Actors
{
    public class MarineMovementComponent : BaseMovementComponent
    {
        public override void DoMove()
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + movement.ToXZ(), Time.deltaTime * speedData.Value);
        }
    }
}
