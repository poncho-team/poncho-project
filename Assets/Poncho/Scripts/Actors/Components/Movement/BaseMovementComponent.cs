﻿using Poncho.Datas;
using UnityEngine;

namespace Poncho.Actors
{
    public abstract class BaseMovementComponent : BaseActorComponent
    {
        protected Vector2 movement;
        protected Rigidbody rb;
        protected SpeedData speedData;

        public override void Setup(BaseActor actor)
        {
            base.Setup(actor);
            rb = GetComponentInChildren<Rigidbody>();
            speedData = actor.DataSet.Get<SpeedData>();
        }

        public virtual void OnMove(Vector2 vector)
        {
            movement = vector;
        }

        public virtual void OnStopMoving()
        {
            movement = Vector2.zero;
        }

        private void FixedUpdate()
        {
            DoMove();
        }

        public abstract void DoMove();
    }
}
