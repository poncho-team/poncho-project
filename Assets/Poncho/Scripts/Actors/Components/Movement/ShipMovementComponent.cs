﻿using NaughtyAttributes;
using Poncho.Datas;
using UnityEngine;
namespace Poncho.Actors
{
    public class ShipMovementComponent : BaseMovementComponent
    {
        [ShowNonSerializedField]
        float currSpeed = 0f;
        SpeedData minSpeedData;
        SpeedData maxSpeedData;

        public override void Setup(BaseActor actor)
        {
            base.Setup(actor);
            speedData = actor.DataSet.Get<SpeedData>("ShipAccelerationData");
            minSpeedData = actor.DataSet.Get<SpeedData>("ShipMinSpeedData");
            maxSpeedData = actor.DataSet.Get<SpeedData>("ShipMaxSpeedData");

        }
        public override void OnStopMoving() { }

        public override void DoMove()
        {
            float y = movement.y < 0 ? movement.y * 5 : movement.y;
            currSpeed += y * Time.deltaTime * speedData.Value;
            currSpeed = Mathf.Clamp(currSpeed, minSpeedData.Value, maxSpeedData.Value);

            float difference = 0;
            if (rb.velocity.magnitude > maxSpeedData.Value)
            {
                difference = rb.velocity.magnitude - maxSpeedData.Value;
            }

            rb.velocity += (transform.forward * currSpeed * Time.deltaTime) - (transform.forward * difference * Time.deltaTime);


        }

    }
}
