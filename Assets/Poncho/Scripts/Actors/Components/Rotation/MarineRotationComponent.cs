﻿using UnityEngine;

namespace Poncho.Actors
{
    public class MarineRotationComponent : BaseRotationComponent
    {
        SpriteRenderer spriteRenderer;
        public override void Setup(BaseActor actor)
        {
            base.Setup(actor);
            spriteRenderer = actor.Visual.GetComponent<SpriteRenderer>();

        }

        protected override void DoRotate()
        {
            if(rotation.x > 0)
            {
                spriteRenderer.flipX = false;
            }
            else if(rotation.x < 0)
            {
                spriteRenderer.flipX = true;
            }
        }
    }
}
