﻿using System;
using UnityEngine;

namespace Poncho.Actors
{
    public abstract class BaseRotationComponent : BaseActorComponent
    {
        protected Vector3 rotation;
        public virtual void OnRotate(Vector3 rot)
        {
            rotation = rot;
        }

        public virtual void OnStopRotation()
        {
            rotation = Vector3.zero;
        }

        private void FixedUpdate()
        {
            DoRotate();
        }

        protected abstract void DoRotate();
    }
}
