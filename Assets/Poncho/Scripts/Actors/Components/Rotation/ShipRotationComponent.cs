﻿using Poncho.Datas;
using UnityEngine;

namespace Poncho.Actors
{
    public class ShipRotationComponent : BaseRotationComponent
    {
        SpeedData turningSpeed;
        public override void Setup(BaseActor actor)
        {
            base.Setup(actor);
            turningSpeed = actor.DataSet.Get<SpeedData>("ShipTurningSpeedData");
        }

        protected override void DoRotate()
        {
            var rot = (this.rotation.x * Vector3.up) * Time.deltaTime * this.turningSpeed.Value;
            transform.Rotate(rot);
        }
    }
}
