﻿using UnityEngine;

namespace Poncho.Actors
{
    public abstract class BaseAnimatorComponent : BaseActorComponent
    {
        protected Animator anim;

        public override void Setup(BaseActor actor)
        {
            base.Setup(actor);
            anim = actor.Visual.GetComponent<Animator>();
        }

        public abstract void OnMove(Vector2 vector);
        public abstract void OnStopMoving();

    }
}
