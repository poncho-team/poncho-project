﻿using UnityEngine;

namespace Poncho.Actors
{
    public class MarineAnimatorComponent : BaseAnimatorComponent
    {
        
        public override void OnStopMoving()
        {
            anim.SetBool("RUN", false);
        }

        public override void OnMove(Vector2 vector)
        {
            if (vector != default) anim.SetBool("RUN", true);
            else anim.SetBool("RUN", false);
        }
    }
}
