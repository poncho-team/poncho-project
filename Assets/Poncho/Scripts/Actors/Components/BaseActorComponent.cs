﻿using UnityEngine;

namespace Poncho.Actors
{
    public abstract class BaseActorComponent : MonoBehaviour
    {
        protected BaseActor actor;
        public virtual void Setup(BaseActor actor)
        {
            this.actor = actor;
        }
    }
}
