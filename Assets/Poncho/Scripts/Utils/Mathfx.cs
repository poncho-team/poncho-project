﻿using UnityEngine;

/// <summary>
/// Opções de Easing pro lerp. 
/// http://wiki.unity3d.com/index.php?title=Mathfx
/// </summary>
public static class Mathfx
{
    /// <summary>
    /// This method will interpolate while easing in and out at the limits.
    /// </summary>
    public static float Hermite(float start, float end, float value)
    {
        value = Mathf.Clamp(value, 0, 1);
        return Mathf.Lerp(start, end, value * value * (3.0f - 2.0f * value));
    }

    /// <summary>
    /// This method will interpolate while easing in and out at the limits.
    /// </summary>
    public static Vector2 Hermite(Vector2 start, Vector2 end, float value)
    {
        return new Vector2(Hermite(start.x, end.x, value), Hermite(start.y, end.y, value));
    }

    /// <summary>
    /// This method will interpolate while easing in and out at the limits.
    /// </summary>
    public static Vector3 Hermite(Vector3 start, Vector3 end, float value)
    {
        return new Vector3(Hermite(start.x, end.x, value), Hermite(start.y, end.y, value), Hermite(start.z, end.z, value));
    }

    /// <summary>
    /// Short for 'sinusoidal interpolation', this method will interpolate while easing around the end, when value is near one.
    /// </summary>
    public static float Sinerp(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, Mathf.Sin(value * Mathf.PI * 0.5f));
    }

    /// <summary>
    /// Short for 'sinusoidal interpolation', this method will interpolate while easing around the end, when value is near one.
    /// </summary>
    public static Vector2 Sinerp(Vector2 start, Vector2 end, float value)
    {
        return new Vector2(Mathf.Lerp(start.x, end.x, Mathf.Sin(value * Mathf.PI * 0.5f)), Mathf.Lerp(start.y, end.y, Mathf.Sin(value * Mathf.PI * 0.5f)));
    }

    /// <summary>
    /// Short for 'sinusoidal interpolation', this method will interpolate while easing around the end, when value is near one.
    /// </summary>
    public static Vector3 Sinerp(Vector3 start, Vector3 end, float value)
    {
        return new Vector3(Mathf.Lerp(start.x, end.x, Mathf.Sin(value * Mathf.PI * 0.5f)), Mathf.Lerp(start.y, end.y, Mathf.Sin(value * Mathf.PI * 0.5f)), Mathf.Lerp(start.z, end.z, Mathf.Sin(value * Mathf.PI * 0.5f)));
    }

    /// <summary>
    /// Similar to Sinerp, except it eases in, when value is near zero, instead of easing out (and uses cosine instead of sine).
    /// </summary>
    public static float Coserp(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, 1.0f - Mathf.Cos(value * Mathf.PI * 0.5f));
    }

    /// <summary>
    /// Similar to Sinerp, except it eases in, when value is near zero, instead of easing out (and uses cosine instead of sine).
    /// </summary>
    public static Vector2 Coserp(Vector2 start, Vector2 end, float value)
    {
        return new Vector2(Coserp(start.x, end.x, value), Coserp(start.y, end.y, value));
    }

    /// <summary>
    /// Similar to Sinerp, except it eases in, when value is near zero, instead of easing out (and uses cosine instead of sine).
    /// </summary>
    public static Vector3 Coserp(Vector3 start, Vector3 end, float value)
    {
        return new Vector3(Coserp(start.x, end.x, value), Coserp(start.y, end.y, value), Coserp(start.z, end.z, value));
    }

    /// <summary>
    /// Short for 'boing-like interpolation', this method will first overshoot, then waver back and forth around the end value before coming to a rest.
    /// </summary>
    public static float Berp(float start, float end, float value)
    {
        value = Mathf.Clamp01(value);
        value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));
        return start + (end - start) * value;
    }

    /// <summary>
    /// Short for 'boing-like interpolation', this method will first overshoot, then waver back and forth around the end value before coming to a rest.
    /// </summary>
    public static Vector2 Berp(Vector2 start, Vector2 end, float value)
    {
        return new Vector2(Berp(start.x, end.x, value), Berp(start.y, end.y, value));
    }

    /// <summary>
    /// Short for 'boing-like interpolation', this method will first overshoot, then waver back and forth around the end value before coming to a rest.
    /// </summary>
    public static Vector3 Berp(Vector3 start, Vector3 end, float value)
    {
        return new Vector3(Berp(start.x, end.x, value), Berp(start.y, end.y, value), Berp(start.z, end.z, value));
    }

    /// <summary>
    /// SmoothStep - Works like Lerp, but has ease-in and ease-out of the values.
    /// </summary>
    public static float SmoothStep(float x, float min, float max)
    {
        x = Mathf.Clamp(x, min, max);
        float v1 = (x - min) / (max - min);
        float v2 = (x - min) / (max - min);
        return -2 * v1 * v1 * v1 + 3 * v2 * v2;
    }

    /// <summary>
    /// SmoothStep - Works like Lerp, but has ease-in and ease-out of the values.
    /// </summary>
    public static Vector2 SmoothStep(Vector2 vec, float min, float max)
    {
        return new Vector2(SmoothStep(vec.x, min, max), SmoothStep(vec.y, min, max));
    }

    /// <summary>
    /// SmoothStep - Works like Lerp, but has ease-in and ease-out of the values.
    /// </summary>
    public static Vector3 SmoothStep(Vector3 vec, float min, float max)
    {
        return new Vector3(SmoothStep(vec.x, min, max), SmoothStep(vec.y, min, max), SmoothStep(vec.z, min, max));
    }

    /// <summary>
    /// Lerp - Short for 'linearly interpolate', this method is equivalent to Unity's Mathf.Lerp, included for comparison.
    /// </summary>
    public static float Lerp(float start, float end, float value)
    {
        return ((1.0f - value) * start) + (value * end);
    }

    /// <summary>
    /// Will return the nearest point on a line to a point. Useful for making an object follow a track.
    /// </summary>
    public static Vector3 NearestPoint(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 lineDirection = Vector3.Normalize(lineEnd - lineStart);
        float closestPoint = Vector3.Dot((point - lineStart), lineDirection);
        return lineStart + (closestPoint * lineDirection);
    }

    /// <summary>
    ///  Works like NearestPoint except the end of the line is clamped.
    /// </summary>
    public static Vector3 NearestPointStrict(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        Vector3 fullDirection = lineEnd - lineStart;
        Vector3 lineDirection = Vector3.Normalize(fullDirection);
        float closestPoint = Vector3.Dot((point - lineStart), lineDirection);
        return lineStart + (Mathf.Clamp(closestPoint, 0.0f, Vector3.Magnitude(fullDirection)) * lineDirection);
    }

    /// <summary>
    /// Returns a value between 0 and 1 that can be used to easily make bouncing GUI items (a la OS X's Dock)
    /// </summary>
    public static float Bounce(float x)
    {
        return Mathf.Abs(Mathf.Sin(6.28f * (x + 1f) * (x + 1f)) * (1f - x));
    }

    /// <summary>
    /// Returns a value between 0 and 1 that can be used to easily make bouncing GUI items (a la OS X's Dock)
    /// </summary>
    public static Vector2 Bounce(Vector2 vec)
    {
        return new Vector2(Bounce(vec.x), Bounce(vec.y));
    }

    /// <summary>
    /// Returns a value between 0 and 1 that can be used to easily make bouncing GUI items (a la OS X's Dock)
    /// </summary>
    public static Vector3 Bounce(Vector3 vec)
    {
        return new Vector3(Bounce(vec.x), Bounce(vec.y), Bounce(vec.z));
    }

    /// <summary>
    /// CLerp - Circular Lerp - is like lerp but handles the wraparound from 0 to 360.
    /// This is useful when interpolating eulerAngles and the object
    /// crosses the 0/360 boundary.  The standard Lerp function causes the object
    /// to rotate in the wrong direction and looks stupid. Clerp fixes that.
    /// </summary>
    public static float Clerp(float start, float end, float value)
    {
        float min = 0.0f;
        float max = 360.0f;
        float half = Mathf.Abs((max - min) / 2.0f);//half the distance between min and max
        float retval = 0.0f;
        float diff = 0.0f;

        if ((end - start) < -half)
        {
            diff = ((max - start) + end) * value;
            retval = start + diff;
        }
        else if ((end - start) > half)
        {
            diff = -((max - end) + start) * value;
            retval = start + diff;
        }
        else retval = start + (end - start) * value;

        // Debug.Log("Start: "  + start + "   End: " + end + "  Value: " + value + "  Half: " + half + "  Diff: " + diff + "  Retval: " + retval);
        return retval;
    }

}
