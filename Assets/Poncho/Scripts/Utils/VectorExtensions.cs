﻿using UnityEngine;


    static class VectorExtensions
    {
        public static Vector3 ToXZ(this Vector2 vec) => new Vector3(vec.x, 0, vec.y);
    }

