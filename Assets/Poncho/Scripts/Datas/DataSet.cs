﻿using Poncho.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Poncho.Datas
{
    [CreateAssetMenu(menuName = "Data/DataSet")]
    public class DataSet : ScriptableObject
    {
        public List<BaseData> Datas = new List<BaseData>();
        
        public T Get<T>() where T : BaseData
        {
            if (Datas.Count <= 0) return null;
            var data = Datas?.FirstOrDefault(x => (x is T)) as T;
            return data;
        }

        public T Get<T>(string name) where T: BaseData
        {
            if (Datas.Count <= 0) return null;
            var data = Datas?.FirstOrDefault(x => (x is T) && x.name == name) as T;
            return data;
        }
    }
}

