﻿using System;
using UnityEngine;

namespace Poncho.Datas
{
    [CreateAssetMenu(menuName = "Data/HealthData")]
    public class HealthData : SingleValueData<float>
    {

        [NonSerialized]
        public float MaxHealth;
        public override void OnAfterDeserialize()
        {
            base.OnAfterDeserialize();
            MaxHealth = InitialValue;

        }
    }


}

