﻿using UnityEngine;

namespace Poncho.Datas
{
    public abstract class BaseData : ScriptableObject, ISerializationCallbackReceiver
    {
        public abstract void OnAfterDeserialize();
        public abstract void OnBeforeSerialize();
    }
}

