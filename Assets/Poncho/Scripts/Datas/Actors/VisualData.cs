﻿using Poncho.Actors;
using UnityEngine;

namespace Poncho.Datas
{
    [CreateAssetMenu(menuName = "Data/Actors/VisualData")]
    public class VisualData : ActorData
    {
        public GameObject Prefab;

        public override void OnAfterDeserialize() { }

        public override void OnBeforeSerialize() { }
    }


}

