﻿using UnityEngine;


namespace Poncho.Datas
{
    [CreateAssetMenu(menuName ="Data/Actors/ActorCameraData")]
    public class ActorCameraData : ActorData
    {
        public float Dampening;
        public float pitchAngle;
        public Vector3 Offset;

        public override void OnAfterDeserialize()
        {

        }

        public override void OnBeforeSerialize()
        {
        }
    }
}

