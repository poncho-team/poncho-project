﻿using System;
using UnityEngine;

namespace Poncho.Datas
{
    
    public abstract class SingleValueData<T> : BaseData
    {
        [SerializeField]
        protected T InitialValue;
        [NonSerialized]
        public T Value;

        public override void OnAfterDeserialize()
        {
             Value = InitialValue;
        }

        public override void OnBeforeSerialize(){}
    }


}

