﻿using UnityEngine;

namespace Poncho.Datas
{
    [CreateAssetMenu(menuName = "Data/SpeedData")]
    public class SpeedData : SingleValueData<float> { }
}